// create an Angular module
// IIFE - immediately invoke function expression 

(function (){
    //create an instance of the application
    var ToDoApp = angular.module("ToDoApp",[]);

    //create controller 
    
    function TodoCtrl(ModalService){
        // save pointer to app instance
        var myToDo = this;

        // initialize variables
            // myToDo.title = "";
            myToDo.description="";
            myToDo.notes = "";
            myToDo.dueDate = "";
            myToDo.priority = "";
            myToDo.category = "";
            myToDo.completed = "completed"; 
            myToDo.reminddays = 0;
            myToDo.dueStatus = "";

            myToDo.taskList =[{
                description:"Go shopping",
                notes: "Let's go shopping at Taka",
                dueDate:"Feb 14, 2017",
                priority:"medium",
                category:"social",
                completed:"completed"
            },
            {
                description:"Go exercise",
                notes: "Let's go exercise at Taka'",
                dueDate: "Feb 14, 2017",
                priority: "medium",
                category:"social",
                completed:"completed"
            }];

            myToDo.filterText = "";
            myToDo.newCategory="";
            myToDo.listHeader ="All Tasks";


            //load to do list 
            myToDo.showAllTasks = function(){
                myToDo.listHeader = "All"
                
            };

            myToDo.showOverDue = function(){
                myToDo.listHeader = "Overdue";
                
            };

            //load items due today
            myToDo.showToday = function () {
                myToDo.listHeader = "Today";
                console.log("Show today");
            };

            //load items due next 7 days 
            myToDo.showThisWeek = function () {
                myToDo.listHeader = "Next 7 Days";
            };

            //load items due this week 
            myToDo.showCompleted = function () {
                myToDo.listHeader = "Completed";
            };

            //controller logic 
            myToDo.addToList = function(){
            //add new 
                myToDo.taskList.push({
                    description: myToDo.description,
                    notes: myToDo.notes,
                    category: myToDo.category,
                    dueDate: myToDo.dueDate,
                    dueStatus: myToDo.dueStatus
                });

                //reset the model
                myToDo.description = "";
                myToDo.notes = "";
                myToDo.category = "";
                myToDo.dueDate = "";
            }
    } //TodoCtrl

    //register controller 

    ToDoApp.controller("TodoCtrl",[TodoCtrl]);

})();