// for client side we use IIFE to execute the application
(function() {
    console.log("Client Side SmartyDo App");
    // everything inside this function is our application
    // Create an instance of Angular application
    //1st param: app name, the second is the app dependencies
    var SmartydoApp = angular.module("SmartydoApp",[]);


    var SmartydoCtrl = function($http, $log) {
        // hold a reference of this controller so that when 'this' changes we are 
        //still referencing the controller
        var smartydoCtrl = this;
        smartydoCtrl.filterText = "";
        smartydoCtrl.filterCat = "";
        smartydoCtrl.listHeader ="";
        smartydoCtrl.taskID = "";
        smartydoCtrl.taskStatus = "pending";
        smartydoCtrl.taskList =[];
        smartydoCtrl.pendingList =[];
        smartydoCtrl.overdueList =[];
        smartydoCtrl.todayList =[];
        smartydoCtrl.next7List =[];
        smartydoCtrl.completedList =[];
        smartydoCtrl.displayList = [];
        smartydoCtrl.today = new Date();

        smartydoCtrl.taskCategory = [];


        smartydoCtrl.taskCategory = [];

        // Data Structure
        // var smartydoData = [{"Name": "Task 1",
        //                      "Desc":"Task 1 Description",
        //                      "Date": "10/02/17",
        //                      "status":"Pending",     : Pending, Completed, Archived 
        //                      "priority":"Low",       : Low, Med, High
        //                      "category": "Work",     : ["Work","Social","Family","Friends","None"] 
        //                      "notify":"No",          : Yes, No
        //                      "email":"email@email.com"
        //                      }];


        // smartydoCtrl.data = [{"Name": "Task 1","Desc":"Task 1 Description",
        //                         "Date": "10/02/17","status":"pending","priority":"low","notify":"no","email":"email@email.com"}];
        
        //categorize task
            smartydoCtrl.catTask = function(){
                var taskDate = [];
                var thisYear ="";
                var thisMonth ="";
                var thisDate ="";
                // var d1 = "";
                var d2 = "";
                var numdays=0;
                var nummth =0;
                var numyr = 0;

                smartydoCtrl.pendingList =[];
                smartydoCtrl.overdueList =[];
                smartydoCtrl.todayList =[];
                smartydoCtrl.next7List =[];
                smartydoCtrl.completedList =[];
                smartydoCtrl.displayList = [];

                console.log("the day today is", smartydoCtrl.today);
                console.log("catTask taskList before allocating.....", smartydoCtrl.taskList);
 
                for (var i = 0; i < smartydoCtrl.taskList.length; i++){
                    
                    // taskDate =new Date(smartydoCtrl.taskList[i].taskdate);

                    // get day, month, year of taskdate 
                    taskDate = smartydoCtrl.taskList[i].taskdate.slice(0,10);
                    taskDate = smartydoCtrl.taskList[i].taskdate.split('/');
                    // d1 = new Date(thisYear, thisMonth, thisDate);

                    // get day, month, year of taskdate + 7
                    d2 = new Date(taskDate[0], taskDate[1], taskDate[2]);

                    // get day, month and year of today
                    thisYear = smartydoCtrl.today.getFullYear();
                    thisMonth = smartydoCtrl.today.getMonth() + 1;
                    thisDate = smartydoCtrl.today.getDate();

                    numdays = (thisDate - taskDate[0]);
                    nummth = (thisMonth - taskDate[1]);
                    numyr = (thisYear - taskDate[2]);
            
                    console.log("number of days", numdays);
                    console.log("number of months", nummth);
                    console.log("number of year", numyr);

                    console.log ("date of day today and taskdate", thisDate, taskDate[0]);
                    console.log ("date of month thismonth and taskdate", thisMonth, taskDate[1]);
                    console.log ("date of year this year and taskdate", thisYear, taskDate[2]);

                    console.log("Before allocation.........", smartydoCtrl.taskList[i] );
                    console.log("Before allocation Status is .........", smartydoCtrl.taskList[i].status );

                    //load pending list 
                    if (smartydoCtrl.taskList[i].status == "pending"){
                        smartydoCtrl.pendingList.push(smartydoCtrl.taskList[i]);
                        console.log("Allocated to pending........", smartydoCtrl.taskList[i] );

                        switch (true){
                        // today
                        case (numyr == 0 && nummth == 0 && numdays == 0 ):
                            smartydoCtrl.todayList.push(smartydoCtrl.taskList[i]);
                            break;
                        // overdue
                        case (numyr == 0 && nummth == 0 &&  numdays > 0): 
                        case (numyr == 0 && nummth > 0):
                        case (numyr < 0):
                            smartydoCtrl.overdueList.push(smartydoCtrl.taskList[i]);
                            break;
                        // 7 days 
                        case (numyr == 0 && nummth == 0 && (numdays < 0 && numdays > - 8)):
                        case (numyr == 0 && nummth == -1 && (d2.getDate() < 7)):
                            smartydoCtrl.next7List.push(smartydoCtrl.taskList[i]);
                            break;
                        //
                    };

                    } else if (smartydoCtrl.taskList[i].status == "completed") {
                        smartydoCtrl.completedList.push(smartydoCtrl.taskList[i]);
                    };
                    
                    // switch (true){
                    //     // today
                    //     case (numyr == 0 && nummth == 0 && numdays == 0 ):
                    //         smartydoCtrl.todayList.push(smartydoCtrl.taskList[i]);
                    //         break;
                    //     // overdue
                    //     case (numyr == 0 && nummth == 0 &&  numdays > 0): 
                    //     case (numyr == 0 && nummth > 0):
                    //     case (numyr < 0):
                    //         smartydoCtrl.overdueList.push(smartydoCtrl.taskList[i]);
                    //         break;
                    //     // 7 days 
                    //     case (numyr == 0 && nummth == 0 && (numdays < 0 && numdays > - 8)):
                    //     case (numyr == 0 && nummth == -1 && (d2.getDate() < 7)):
                    //         smartydoCtrl.next7List.push(smartydoCtrl.taskList[i]);
                    //         break;
                    //     //
                    // };
                };
                    console.log("Pending list...", smartydoCtrl.pendingList);
                    console.log("Completed list...", smartydoCtrl.completedList);
                    console.log("Today list...", smartydoCtrl.todayList);
                    console.log("OverDue list...", smartydoCtrl.overdueList);
                    console.log("Next 7 list...", smartydoCtrl.next7List);

                    smartydoCtrl.displayList = [];
                    console.log("header name", smartydoCtrl.listHeader);
                    switch (smartydoCtrl.listHeader) {
                        case "":
                            smartydoCtrl.listHeader = "All Pending";
                            smartydoCtrl.displayList = smartydoCtrl.pendingList;
                            break;
                        case "All Pending":
                            smartydoCtrl.displayList = smartydoCtrl.pendingList;
                            break;
                        case "Overdue":
                             smartydoCtrl.displayList = smartydoCtrl.overdueList;
                            break;
                        case "Today":
                            smartydoCtrl.displayList = smartydoCtrl.todayList;
                            break;
                        case "Next 7 Days":
                            smartydoCtrl.displayList = smartydoCtrl.next7List;
                            break;
                    };
                    console.log("pending list.......",smartydoCtrl.pendingList);
            };


        //load to do list 

            smartydoCtrl.showAllPending = function(){
                // smartydoCtrl.getAllTask();
                // smartydoCtrl.displayList = [];
                smartydoCtrl.displayList = smartydoCtrl.pendingList;
                smartydoCtrl.listHeader = "All Pending";
                // smartydoCtrl.filterText = "Pending";
                
            };

            smartydoCtrl.showOverDue = function(){
                smartydoCtrl.listHeader = "Overdue";
                smartydoCtrl.displayList = smartydoCtrl.overdueList;
                
            };

            //load items due today
            smartydoCtrl.showToday = function () {
                smartydoCtrl.listHeader = "Today";
                smartydoCtrl.displayList = smartydoCtrl.todayList;
                console.log("Show today");
            };

            //load items due next 7 days 
            smartydoCtrl.showThisWeek = function () {
                smartydoCtrl.listHeader = "Next 7 Days";
                smartydoCtrl.displayList = smartydoCtrl.next7List;
            };

            //load items due this week 
            smartydoCtrl.showCompleted = function () {
                smartydoCtrl.listHeader = "Completed";
                // smartydoCtrl.filterText = "Completed";
                smartydoCtrl.displayList = smartydoCtrl.completedList;
            };

            //Add a new task
            smartydoCtrl.addTask = function () {

                //generate unique ID
               smartydoCtrl.taskID = "SMTDL" + parseInt(Math.floor(Date.now() / 1000)) ;
               smartydoCtrl.createTaskSave();

                // smartydoCtrl.getAllTask();
                // console.log("task List after get all task", smartydoCtrl.taskList);
                // console.log("task List after pushing new Task", smartydoCtrl.taskList);

            };

            // updated the following function

            //Mark Task as complete
            smartydoCtrl.markComplete = function (returnObject) {

                //generate unique ID
               returnObject.status = "completed";
            //    console.log(JSON.stringify(returnObject)) ;
               smartydoCtrl.updateTask(returnObject);
               
            };

           //Mark Task as deleted
            smartydoCtrl.markDelete = function (returnObject) {

                //generate unique ID
               returnObject.status = "deleted";

            //    console.log(JSON.stringify(returnObject)) ;
               smartydoCtrl.updateTask(returnObject);
               
            };

        
    var createTaskObject = function(ctrl){
        console.log("ctrl status.....", ctrl.tmpTaskDate);
            if (ctrl.tmpTaskCate != "none"){
                ctrl.tmpTaskCate = "none";
            };
           return ({
               id : ctrl.taskID,
               name : ctrl.tmpTaskName,
               taskdate : ctrl.tmpTaskDate.toLocaleDateString(),
               description : ctrl.tmpTaskDesc,
               priority : ctrl.tmpTaskPrio,
               category : ctrl.tmpTaskCate,
               notify : ctrl.tmpTaskNoti,
               status : "pending"
           });
       };

       var initTask = function (ctrl) {
               ctrl.tmpTaskId = "";
               ctrl.tmpTaskName = "";
               ctrl.tmpTaskDate = "";
               ctrl.tmpTaskDesc = "";
               ctrl.tmpTaskPrio = "low";
               ctrl.tmpTaskCate = "none";
               ctrl.tmpTaskNoti = "no";
               ctrl.tmpTaskStatus = "pending";
       } ;


       // Initialize environment

    //    var Initialize = function (){

    //         console.log("Initialize databases, get data from server");

    //         console.log("getalltask");
    //         $http.get("/getalltask") 
    //             .then(function(result) {
    //                 smartydoCtrl.taskList = result.data;
    //                 // smartydoCtrl.taskList.push(JSON.stringify(result.data));
    //                 console.log("taskList---> " + JSON.stringify(smartydoCtrl.taskList))  ;
    //                 // return (result.data);
    //             });

    //         console.log("getcategory");
    //         $http.get("/getcategory") 
    //             .then(function(result) {
    //                 smartydoCtrl.taskCategory = result.data;
    //                 // smartydoCtrl.taskList.push(JSON.stringify(result.data));
    //                 console.log("taskList---> " + JSON.stringify(smartydoCtrl.taskCategory))  ;
    //                 // return (result.data);
    //             });


    //    };

    //    Initialize();

        smartydoCtrl.initialize = function(){

            smartydoCtrl.getAllTask();
            smartydoCtrl.getCategory();

        };

        smartydoCtrl.createTaskSave = function() {
           // initTask(smartydoCtrl);                       
        //    smartydoCtrl.tmpTaskId = "SMTDL" + parseInt(Math.floor(Date.now() / 1000)) ;
            console.log("before creating object**********************", smartydoCtrl);
           $http.get("/addtask",{
               params : createTaskObject(smartydoCtrl)
           }).then(function(result){
                initTask(smartydoCtrl);
                console.log(result);
                // reload data from server after adding task
               smartydoCtrl.getAllTask();  
               //console.log(smartydoCtrl.taskList);
            });

        };

// change the following from complete task to updatetask and remove the separate update task since property update is handeled above

    smartydoCtrl.updateTask = function(completedTask){
           console.log("Complete Task", JSON.stringify(completedTask)) ;

           $http.get("/updatetask",{
               params : completedTask
           }).then(function(){
                initTask(smartydoCtrl);
                // console.log("display list in update", smartydoCtrl.displayList);
                // reload data from server after adding task
                smartydoCtrl.getAllTask();  
                // console.log("display list in update after get all", smartydoCtrl.displayList);
               console.log(JSON.stringify(smartydoCtrl.taskList)); // just for debugging
            }).catch(function(){ 
               console.log("Error updating task", JSON.stringify(smartydoCtrl[idx])) ; // just for debugging;          
       });
    };


        smartydoCtrl.getAllTask = function(){
            console.log("getalltask");
            $http.get("/getalltask") 
                .then(function(result) {
                    smartydoCtrl.taskList = result.data;
                    console.log("taskList before ---> " + JSON.stringify(smartydoCtrl.taskList))  ;
                    smartydoCtrl.catTask();
                    // smartydoCtrl.taskList.push(JSON.stringify(result.data));
                    console.log("taskList after ---> " + JSON.stringify(smartydoCtrl.taskList))  ;
                    // return (result.data);
                });
        };

        smartydoCtrl.getCategory = function(){
                console.log("getcategory");
                $http.get("/getcategory") 
                    .then(function(result) {
                        smartydoCtrl.taskCategory = result.data;
                        // smartydoCtrl.taskList.push(JSON.stringify(result.data));
                        console.log("taskList---> " + JSON.stringify(smartydoCtrl.taskCategory))  ;
                        // return (result.data);
                    });

        };


        smartydoCtrl.updateCategory = function(category){
            console.log("Add/update category", JSON.stringify(category)) ;

            $http.get("/updatecategory",{
                params : category
            }).then(function(){
                console.log(JSON.stringify(category)); // just for debugging
            }).catch(function(){ 
                console.log("Error updating task", JSON.stringify(category)) ; // just for debugging;          
            });
        };
               smartydoCtrl.getCategory = function(){
                console.log("getcategory");
                $http.get("/getcategory") 
                    .then(function(result) {
                        smartydoCtrl.taskCategory = result.data;
                        // smartydoCtrl.taskList.push(JSON.stringify(result.data));
                        console.log("taskList---> " + JSON.stringify(smartydoCtrl.taskCategory))  ;
                        // return (result.data);
                    });

        };


    smartydoCtrl.updateCategory = function(category){
           console.log("Add/update category", JSON.stringify(category)) ;

           $http.get("/updatecategory",{
               params : category
           }).then(function(){
               console.log(JSON.stringify(category)); // just for debugging
           }).catch(function(){
               console.log("Error updating task", JSON.stringify(category)) ; // just for debugging;          
           });
       };


    };
    //Define a controller call FirstCtrl -
    SmartydoApp.controller("SmartydoCtrl",["$http", "$log", SmartydoCtrl ]);

})();