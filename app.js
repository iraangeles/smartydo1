//load libraries
express = require("express");
path = require("path"); 

//create an instance of the app
app = express();

//configure routes
app.use(express.static(path.join(__dirname, "public")));
app.use("/libs", express.static(path.join(__dirname, "bower_components")));

//set port 
app.set("port", parseInt(process.argv[2]) || process.env.APP_PORT || 3000);

//start up app server
app.listen(app.get("port"), function (){
    console.log("Server started on port %s", app.get("port"));
})
